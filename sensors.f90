program sensors

INTEGER i, N, t, tfin, numbantennas,packages
INTEGER transmittedpackages,transmittedpackagesreal,r,maxreal,seed
REAL*8 x,y,a,dir,v,posantennas,radio,transmittedpackagestot
CHARACTER (1024) paquetes1
PARAMETER (N=500) 
PARAMETER (tfin=20000)
PARAMETER (maxreal=10) 
PARAMETER (numbantennas=10)
PARAMETER (radio=0.01)
PARAMETER (a=0.01)
PARAMETER (v=0.001)
dimension x(N,tfin) 
dimension y(N,tfin)
dimension posantennas(numbantennas,2) 
dimension packages(N) 
dimension transmittedpackages(N)
dimension transmittedpackagesreal(maxreal)
REAL, PARAMETER :: Pi = 3.1415927

write (paquetes1,'(a10,a4)') 'paquetes1','.dat' 
open (10,file=trim(paquetes1),access='append')

do r=1,maxreal
transmittedpackagesreal(r)=0 
enddo

do r=1,maxreal

do i=1,N
packages(i)=0
transmittedpackages(i)=0
end do

do i=1,N
CALL random_seed(seed)
CALL RANDOM_NUMBER(x(i,1))
CALL random_seed(seed)
CALL RANDOM_NUMBER(y(i,1))
enddo

do i=1,numbantennas
CALL random_seed(seed)
CALL RANDOM_NUMBER(posantennas(i,1))
CALL random_seed(seed)
CALL RANDOM_NUMBER(posantennas(i,2))
enddo

DO i=1,N 
DO  t = 2, tfin 
call random_seed(seed)
call random_number(dir) 
x(i,t) = x(i,t-1) + v*cos(2*Pi*dir) - a*(x(i,t-1)-x(i,1))
y(i,t) = y(i,t-1) + v*sin(2*Pi*dir) - a*(y(i,t-1)-y(i,1))
packages(i)=packages(i)+1
do j=1,numbantennas
if (sqrt((x(i,t)-posantennas(j,1))**2 + (y(i,t)-posantennas(j,2))**2) .lt. radio) then
transmittedpackages(i)=transmittedpackages(i)+packages(i) 
packages(i)=0
end if 
end do 
end do
ENDDO

do i=1,N
transmittedpackagesreal(r)=transmittedpackagesreal(r)+transmittedpackages(i)
enddo

enddo !en r

transmittedpackagestot=sum(transmittedpackagesreal)/maxreal

write(10,'(i4,i7,f12.5)') numbantennas,tfin,transmittedpackagestot/tfin/N 

END !program

